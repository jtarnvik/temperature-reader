/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.pricer.temperaturetopfi;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

public class CalendarGenerator {

   private final static int BOX_X_SIZE = 30;
   private final static int BOX_Y_SIZE = 35;
   private final static int CAL_SIZE = 250;

   @Data
   public static class CalendarInfo {

      private String day;
      private int dayNumber;
      private int weekNumber;
      private String month;
      private int monthNumber;
      private BufferedImage monthImage;
   }

   public static CalendarInfo generateCalendarInfo() {
      Locale SWEDEN = new Locale("sv");

      GregorianCalendar now = new GregorianCalendar(SWEDEN);

      CalendarInfo info = new CalendarInfo();
      info.setDay(StringUtils.capitalize(new SimpleDateFormat("EEEEEEEEE", SWEDEN).format(now.getTime())));
      info.setMonth(StringUtils.capitalize(new SimpleDateFormat("MMMMMMMMMM", SWEDEN).format(now.getTime())));
      info.setMonthNumber(now.get(Calendar.MONTH) + 1);
      info.setDayNumber(now.get(Calendar.DAY_OF_MONTH));
      info.setWeekNumber(now.get(Calendar.WEEK_OF_YEAR));

      BufferedImage img = new BufferedImage(CAL_SIZE, CAL_SIZE, BufferedImage.TYPE_INT_ARGB);
      Graphics2D gr = img.createGraphics();
      gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

      gr.setBackground(Color.WHITE);
      gr.setColor(Color.BLACK);
      gr.clearRect(0, 0, CAL_SIZE, CAL_SIZE);

      gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, null, 0f));
      Font font = new Font("SansSerif", Font.PLAIN, 10);
      gr.setFont(font);

      GregorianCalendar firstDay = new GregorianCalendar(SWEDEN);
      firstDay.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), 1);
//      GregorianCalendar firstDay = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), 1, 10, 10);

      int currentMonth = firstDay.get(Calendar.MONTH);
      GregorianCalendar currentDay = firstDay;

      for (int i = 0; i < 7; ++i) {
         int boxX = 0;
         int boxY = CAL_SIZE - BOX_Y_SIZE * i - BOX_Y_SIZE;
         int week = firstDay.get(Calendar.WEEK_OF_YEAR) + i;
         DrawUtils.drawString(gr, boxX, boxY, CAL_SIZE, "" + week);
         System.out.println("Printing week: " + week);

      }

      int currentRow = 0;
      while (currentDay.get(Calendar.MONTH) == currentMonth) {
         int currentCol = getCol(currentDay.get(Calendar.DAY_OF_WEEK)) + 1;
         System.out.println("" + currentDay.getTime() + " --- row: " + currentRow + " col: " + currentCol);
         int boxX = BOX_X_SIZE * currentCol;
         int boxY = CAL_SIZE - BOX_Y_SIZE * currentRow - BOX_Y_SIZE;

         DrawUtils.drawString(gr, boxX, boxY, CAL_SIZE, "" + currentDay.get(Calendar.DAY_OF_MONTH));

         currentDay.add(Calendar.DAY_OF_MONTH, 1);
         if (currentDay.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
            currentRow++;
         }
      }

      info.setMonthImage(img);
      gr.dispose();
      return info;
   }

   public static int getCol(int day) {
      switch (day) {
         case Calendar.MONDAY:
            return 0;
         case Calendar.TUESDAY:
            return 1;
         case Calendar.WEDNESDAY:
            return 2;
         case Calendar.THURSDAY:
            return 3;
         case Calendar.FRIDAY:
            return 4;
         case Calendar.SATURDAY:
            return 5;
         case Calendar.SUNDAY:
            return 6;
         default:
            throw new IllegalArgumentException("Thats not a day!");
      }
   }

   public static void main(String[] args) {
      CalendarInfo info = generateCalendarInfo();
      DrawUtils.writeImageToDisk(info.getMonthImage(), "c:\\tmp.png");

   }
}
