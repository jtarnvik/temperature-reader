/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.pricer.temperaturetopfi;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import lombok.Data;

public class ChartGenerator {

   private static final int EDGE_PADDING = 10;
   private static final int GRAPH_LINE_AREA = 15;
   private static final int BAR_SPACE = 2;
   private static final int BAR_WIDTH = 7;
   private static final int PIXELS_PER_TENTHS = 4;
   private static final int MAX_HEIGHT = 150;
   private static final int LARGE_TICK_HALF_SIZE = 4;
   private static final int SMALL_TICK_HALF_SIZE = 2;
   private static final int MAX_BARS = 38;
   private static final int PIXELS_PER_DEGREE = 8;

   private static final Locale SWEDEN = new Locale("sv");
   private static final DateFormat HOURLY_DATE_FORMAT = new SimpleDateFormat("HH", SWEDEN);
   private static final DateFormat GENERATED_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd HH:00", SWEDEN);

   @Data
   public static class BarData {

      private List<Precipation> pres = new ArrayList<>(24);
      private List<Forecast> temp = new ArrayList<>(24);
      private List<Date> time = new ArrayList<>(24);

      public void addPoint(Precipation pre, Date date, Forecast f) {
         pres.add(pre);
         time.add(date);
         temp.add(f);
      }

   }

   public static BufferedImage getPrecipitationChart(int width, int height, BarData bd) {
      BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
      Graphics2D gr = img.createGraphics();
      gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

      gr.setBackground(Color.WHITE);
      gr.setColor(Color.BLACK);
      gr.clearRect(0, 0, width, height);

      gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, null, 0f));

      // Y - Axis
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA, EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA, height - EDGE_PADDING,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA, height - EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA - 5,
              height - EDGE_PADDING - 5,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA, height - EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA + 5,
              height - EDGE_PADDING - 5,
              height);
      // Large ticks
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - SMALL_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 5,
              EDGE_PADDING + GRAPH_LINE_AREA + SMALL_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 5,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - LARGE_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 10,
              EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 10,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - SMALL_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 15,
              EDGE_PADDING + GRAPH_LINE_AREA + SMALL_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 15,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - LARGE_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 20,
              EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 20,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - SMALL_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 25,
              EDGE_PADDING + GRAPH_LINE_AREA + SMALL_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 25,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - LARGE_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 30,
              EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 30,
              height);

      Font font = new Font("SansSerif", Font.PLAIN, 10);
      gr.setFont(font);
      DrawUtils.drawString(gr, EDGE_PADDING - 5, height - EDGE_PADDING, height, "mm");
      font = new Font("SansSerif", Font.BOLD, 12);
      gr.setFont(font);
      DrawUtils.drawString(gr, EDGE_PADDING - 7, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 10 - 5, height, "1.0");
      DrawUtils.drawString(gr, EDGE_PADDING - 7, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 20 - 5, height, "2.0");
      DrawUtils.drawString(gr, EDGE_PADDING - 7, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 30 - 5, height, "3.0");
      font = new Font("SansSerif", Font.PLAIN, 10);
      gr.setFont(font);

      // X - Axis
      gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, null, 0f));
      DrawUtils.drawLine(gr, EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA, width - EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA,
              height);
      DrawUtils.drawLine(gr, width - EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA, width - EDGE_PADDING - 5,
              EDGE_PADDING + GRAPH_LINE_AREA - 5,
              height);
      DrawUtils.drawLine(gr, width - EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA, width - EDGE_PADDING - 5,
              EDGE_PADDING + GRAPH_LINE_AREA + 5,
              height);
      gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, new float[]{3f, 6f}, 0f));
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 10,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 10, height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 20,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 20, height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 30,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 30, height);
      gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, new float[]{1f, 6f}, 0f));
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 5,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 5, height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 15,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 15, height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 25,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 25, height);
      DrawUtils.drawString(gr, width - EDGE_PADDING - 5, EDGE_PADDING + GRAPH_LINE_AREA - 15, height, "h");

      gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, null, 0f));
      int maxNumBars = (bd.getPres().size() > MAX_BARS) ? MAX_BARS : bd.getPres().size();
      for (int i = 0; i < maxNumBars; ++i) {
         Precipation pre = bd.getPres().get(i);
         Date time = bd.getTime().get(i);

         int barLeft = EDGE_PADDING + GRAPH_LINE_AREA + i * (BAR_SPACE + BAR_WIDTH);
         DrawUtils.fillRect(gr, barLeft + BAR_SPACE, EDGE_PADDING + GRAPH_LINE_AREA, BAR_WIDTH, getBarHeight(pre), height);
         if (HOURLY_DATE_FORMAT.format(time).equals("00")) {
            DrawUtils.drawLine(gr, barLeft + BAR_SPACE + BAR_WIDTH / 2, 5, barLeft + BAR_SPACE + BAR_WIDTH / 2,
                    (int) EDGE_PADDING + GRAPH_LINE_AREA - 10, height);

            DrawUtils.drawLine(gr, barLeft + BAR_SPACE + BAR_WIDTH / 2 + 1, 5, barLeft + BAR_SPACE + BAR_WIDTH / 2 + 1,
                    (int) EDGE_PADDING + GRAPH_LINE_AREA - 10 - 4, height);
            DrawUtils.drawLine(gr, barLeft + BAR_SPACE + BAR_WIDTH / 2 + 2, 5, barLeft + BAR_SPACE + BAR_WIDTH / 2 + 2,
                    (int) EDGE_PADDING + GRAPH_LINE_AREA - 10 - 8, height);

            DrawUtils.drawLine(gr, barLeft + BAR_SPACE + BAR_WIDTH / 2 - 1, 5, barLeft + BAR_SPACE + BAR_WIDTH / 2 - 1,
                    (int) EDGE_PADDING + GRAPH_LINE_AREA - 10 - 4, height);
            DrawUtils.drawLine(gr, barLeft + BAR_SPACE + BAR_WIDTH / 2 - 2, 5, barLeft + BAR_SPACE + BAR_WIDTH / 2 - 2,
                    (int) EDGE_PADDING + GRAPH_LINE_AREA - 10 - 8, height);
         }
         if (i % 2 == 0) {
            DrawUtils.drawString(gr, barLeft, EDGE_PADDING + GRAPH_LINE_AREA - 10, height, HOURLY_DATE_FORMAT.format(time));
         }
      }
      DrawUtils.drawString(gr, EDGE_PADDING + GRAPH_LINE_AREA + 20, height - EDGE_PADDING, height, GENERATED_DATE_FORMAT.format(
              new Date()));

      gr.dispose();
      return img;
   }

   private static int getBarHeight(Precipation pre) {
      float preF = Float.parseFloat(pre.getPrecipation());
      int tenths = (int) (preF * 10);
      int pixels = tenths * PIXELS_PER_TENTHS;
      if (pixels > MAX_HEIGHT) {
         return MAX_HEIGHT;
      } else {
         return pixels;
      }
   }

   public static void main(String[] args) {
      BarData bd = new BarData();

      GregorianCalendar now = new GregorianCalendar();

      for (int i = 0; i < 100; ++i) {
         GregorianCalendar nextXHour = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
                 now.get(Calendar.DAY_OF_MONTH), now.get(Calendar.HOUR_OF_DAY), 0);
         nextXHour.add(Calendar.HOUR, i);
         String mils = String.format("%02d", i);
         if (i == 0) {
            mils = "17.0";
         } else if (i == 1) {
            mils = "26.0";
         } else if (i == 2) {
            mils = "26.5";
         } else if (i == 3) {
            mils = "27.0";
         } else {
            mils = "22.0";
         }
//         mils = mils.charAt(0) + "." + mils.charAt(1);

         bd.addPoint(new Precipation("RAIN", mils), nextXHour.getTime(), new Forecast(Float.parseFloat(mils), "", "",
                 nextXHour.getTime()));
      }

      DrawUtils.writeImageToDisk(getTemperatureChart(400, 179, bd), "c:\\tmp.png");
   }

   public static BufferedImage getTemperatureChart(int width, int height, BarData bd) {
      List<Forecast> sortedTemps = new ArrayList<>();
      int maxNumBars = (bd.getPres().size() > MAX_BARS) ? MAX_BARS : bd.getPres().size();
      for (int i = 0; i < maxNumBars; ++i) {
         sortedTemps.add(bd.getTemp().get(i));
      }
      Collections.sort(sortedTemps);
      int min = (int) Float.parseFloat(sortedTemps.get(0).getTemperature());
      int max = (int) Float.parseFloat(sortedTemps.get(MAX_BARS - 1).getTemperature());

      int lowerScale = (min / 5) * 5;

      BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
      Graphics2D gr = img.createGraphics();
      gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

      gr.setBackground(Color.WHITE);
      gr.setColor(Color.BLACK);
      gr.clearRect(0, 0, width, height);

      gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, null, 0f));

      // Y - Axis
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA, EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA, height - EDGE_PADDING,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA, height - EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA - 5,
              height - EDGE_PADDING - 5,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA, height - EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA + 5,
              height - EDGE_PADDING - 5,
              height);
      // Large ticks
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - LARGE_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA,
              EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA, height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - SMALL_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 5,
              EDGE_PADDING + GRAPH_LINE_AREA + SMALL_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 5,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - LARGE_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 10,
              EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 10,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - SMALL_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 15,
              EDGE_PADDING + GRAPH_LINE_AREA + SMALL_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 15,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - LARGE_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 20,
              EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 20,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - SMALL_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 25,
              EDGE_PADDING + GRAPH_LINE_AREA + SMALL_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 25,
              height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA - LARGE_TICK_HALF_SIZE,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 30,
              EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 30,
              height);

      Font font = new Font("SansSerif", Font.PLAIN, 10);
      gr.setFont(font);
      DrawUtils.drawString(gr, EDGE_PADDING - 5, height - EDGE_PADDING, height, "°C");
      font = new Font("SansSerif", Font.BOLD, 12);
      gr.setFont(font);
      DrawUtils.drawString(gr, EDGE_PADDING - 5, EDGE_PADDING + GRAPH_LINE_AREA - 4, height, "" + lowerScale);
      DrawUtils.drawString(gr, EDGE_PADDING - 5, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_DEGREE * 5 * 1 - 5, height,
              "" + (lowerScale + 5 * 1));
      DrawUtils.drawString(gr, EDGE_PADDING - 5, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_DEGREE * 5 * 2 - 5, height,
              "" + (lowerScale + 5 * 2));
      DrawUtils.drawString(gr, EDGE_PADDING - 5, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_DEGREE * 5 * 3 - 5, height,
              "" + (lowerScale + 5 * 3));
      font = new Font("SansSerif", Font.PLAIN, 10);
      gr.setFont(font);

      // X - Axis
      gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, null, 0f));
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA, EDGE_PADDING + GRAPH_LINE_AREA, width - EDGE_PADDING,
              EDGE_PADDING + GRAPH_LINE_AREA,
              height);
      DrawUtils.drawLine(gr, width - EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA, width - EDGE_PADDING - 5,
              EDGE_PADDING + GRAPH_LINE_AREA - 5,
              height);
      DrawUtils.drawLine(gr, width - EDGE_PADDING, EDGE_PADDING + GRAPH_LINE_AREA, width - EDGE_PADDING - 5,
              EDGE_PADDING + GRAPH_LINE_AREA + 5,
              height);
      gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, new float[]{3f, 6f}, 0f));
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 10,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 10, height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 20,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 20, height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 30,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 30, height);
      gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, new float[]{1f, 6f}, 0f));
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 5,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 5, height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 15,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 15, height);
      DrawUtils.drawLine(gr, EDGE_PADDING + GRAPH_LINE_AREA + LARGE_TICK_HALF_SIZE * 2,
              EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 25,
              width - EDGE_PADDING - 10, EDGE_PADDING + GRAPH_LINE_AREA + PIXELS_PER_TENTHS * 25, height);
      DrawUtils.drawString(gr, width - EDGE_PADDING - 5, EDGE_PADDING + GRAPH_LINE_AREA - 15, height, "h");

      gr.setStroke(new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, null, 0f));
      for (int i = 0; i < maxNumBars - 1; ++i) {
         Date time = bd.getTime().get(i);
         Forecast f0 = bd.getTemp().get(i);
         Forecast f1 = bd.getTemp().get(i + 1);

         float t0 = f0.getTempFloat() - lowerScale;
         float t1 = f1.getTempFloat() - lowerScale;

         float y0 = t0 * PIXELS_PER_DEGREE + EDGE_PADDING + GRAPH_LINE_AREA;
         float y1 = t1 * PIXELS_PER_DEGREE + EDGE_PADDING + GRAPH_LINE_AREA;

         int x0 = EDGE_PADDING + GRAPH_LINE_AREA + i * (BAR_SPACE + BAR_WIDTH) + BAR_WIDTH / 2;
         int x1 = EDGE_PADDING + GRAPH_LINE_AREA + (i + 1) * (BAR_SPACE + BAR_WIDTH) + BAR_WIDTH / 2;

         DrawUtils.drawLine(gr, x0, (int) y0, x1, (int) y1, height);

         int barLeft = EDGE_PADDING + GRAPH_LINE_AREA + i * (BAR_SPACE + BAR_WIDTH);
         if (HOURLY_DATE_FORMAT.format(time).equals("00")) {
            gr.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, null, 0f));
            DrawUtils.drawLine(gr, barLeft + BAR_SPACE + BAR_WIDTH / 2, 5, barLeft + BAR_SPACE + BAR_WIDTH / 2,
                    (int) EDGE_PADDING + GRAPH_LINE_AREA - 10, height);

            DrawUtils.drawLine(gr, barLeft + BAR_SPACE + BAR_WIDTH / 2 + 1, 5, barLeft + BAR_SPACE + BAR_WIDTH / 2 + 1,
                    (int) EDGE_PADDING + GRAPH_LINE_AREA - 10 - 4, height);
            DrawUtils.drawLine(gr, barLeft + BAR_SPACE + BAR_WIDTH / 2 + 2, 5, barLeft + BAR_SPACE + BAR_WIDTH / 2 + 2,
                    (int) EDGE_PADDING + GRAPH_LINE_AREA - 10 - 8, height);

            DrawUtils.drawLine(gr, barLeft + BAR_SPACE + BAR_WIDTH / 2 - 1, 5, barLeft + BAR_SPACE + BAR_WIDTH / 2 - 1,
                    (int) EDGE_PADDING + GRAPH_LINE_AREA - 10 - 4, height);
            DrawUtils.drawLine(gr, barLeft + BAR_SPACE + BAR_WIDTH / 2 - 2, 5, barLeft + BAR_SPACE + BAR_WIDTH / 2 - 2,
                    (int) EDGE_PADDING + GRAPH_LINE_AREA - 10 - 8, height);
            gr.setStroke(new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, null, 0f));
         }
         if (i % 2 == 0) {
            DrawUtils.drawString(gr, barLeft, EDGE_PADDING + GRAPH_LINE_AREA - 10, height, HOURLY_DATE_FORMAT.format(time));
         }
      }
      DrawUtils.drawString(gr, EDGE_PADDING + GRAPH_LINE_AREA + 20, height - EDGE_PADDING, height, GENERATED_DATE_FORMAT.format(
              new Date()));

      gr.dispose();
      return img;
   }

}
