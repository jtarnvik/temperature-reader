/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.pricer.temperaturetopfi;

import lombok.Data;

@Data
public class FullForecast {

   private String nextHourSymbol;
   private String currentTemp;
   private String nextHourWind;
   private String nextHourPrecipitation;

   private String forecastTemp;
   private String forecastWind;
   private String forecastSymbol;
   private String forecastTimes;

   private String forecastHourlyTemp;
   private String forecastHourlySymbol;
   private String forecastHourlyTimes;
   private String forecastHourlyPrecipitation;

   private String hourlyPrecipitationChart;
   private String hourlyTemperatureChart;

}
