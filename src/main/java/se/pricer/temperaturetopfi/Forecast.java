/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.pricer.temperaturetopfi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

@EqualsAndHashCode(of = "forecastTime")
@Data
public class Forecast implements Comparable<Forecast> {

   private static final Locale SWEDEN = new Locale("sv");
   private static final DateFormat DAY_DATE_FORMAT = new SimpleDateFormat("EEE d", SWEDEN);
   private static final DateFormat HOURLY_DATE_FORMAT = new SimpleDateFormat("EEE HH", SWEDEN);
   private String temperature;
   private float tempFloat;
   private String windDir;
   private String windSpeed;
   private String timeForDailyPres;
   private String timeForHourlyPres;
   private Date forecastTime;

   public Forecast(float temperature, String windDir, String windSpeed, Date time) {
      this.temperature = Integer.toString(Math.round(temperature));
      this.tempFloat = temperature;
      this.windDir = windDir;
      this.windSpeed = windSpeed;
      this.timeForDailyPres = StringUtils.capitalize(DAY_DATE_FORMAT.format(time));
      this.timeForHourlyPres = StringUtils.capitalize(HOURLY_DATE_FORMAT.format(time));
      this.forecastTime = time;
   }

   @Override
   public int compareTo(Forecast o) {
      return (new Float(temperature)).compareTo(new Float(o.getTemperature()));
   }

}
