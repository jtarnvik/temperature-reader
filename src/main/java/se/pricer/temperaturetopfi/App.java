package se.pricer.temperaturetopfi;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class App {

   private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
   private static Map<String, String> windTranslations = new HashMap<String, String>() {
      {
         put("N", "N");
         put("NE", "NO");
         put("E", "O");
         put("SE", "SO");
         put("S", "S");
         put("SW", "SV");
         put("W", "V");
         put("NW", "NV");
      }
   };

   public static void main(String[] args) {
      try {
         String location = "hasselby_strand";
         if (args.length > 0) {
            location = args[0];
         }
         CurrentData currentData = getCurrentTemp(location);
         //59.370769, 17.906271 Rösväagen
         //      FullForecast ff = getPrognosis("lat=59.33147;lon=18.05356"); centralen
         FullForecast ff = getPrognosis("lat=59.370769;lon=17.906271");
         if (currentData.getTemp() == null) {
            currentData.setTemp(Integer.parseInt(ff.getCurrentTemp()));
         }
         writePfi(currentData, ff, "Spånga");

      } catch (Exception ex) {
         Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
      }
   }

   private static CurrentData getCurrentTemp(String location) throws NumberFormatException, IOException, ParserConfigurationException, DOMException, SAXException {
      URL url = new URL("http://api.temperatur.nu/tnu_1.12.php?p=" + location + "&verbose&cli=pfi_temp10");

      DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

      DocumentBuilder builder = builderFactory.newDocumentBuilder();
      Document doc = builder.parse(url.openStream());

      Element rss = doc.getDocumentElement();
      Element channel = (Element) rss.getElementsByTagName("channel").item(0);
      Element item = (Element) channel.getElementsByTagName("item").item(0);
      Element temp = (Element) item.getElementsByTagName("temp").item(0);
      Element lastUpdateElem = (Element) item.getElementsByTagName("lastUpdate").item(0);

      Integer temperature;
      String tempString = temp.getTextContent();
      String lastUpdate = lastUpdateElem.getTextContent();
      try {
         temperature = Math.round(Float.parseFloat(tempString));
      } catch (NumberFormatException e) {
         temperature = null;
      }
      return new CurrentData(temperature, lastUpdate);
   }

   private static void writePfi(CurrentData cd, FullForecast ff, String loc) throws Exception {

      String pricerHome = System.getenv("PRICER_HOME");

      File pfiHome = new File(pricerHome, "PFIFiles");

      File dataFiles = new File(pfiHome, "DataFiles");
      File messageFiles = new File(pfiHome, "MessageFiles");
      File resultFiles = new File(pfiHome, "ResultFiles");

      File dataFile = new File(dataFiles, "temp.i1");
      File messageFile = new File(messageFiles, "temp.m1");
      File resultFile = new File(resultFiles, "temp.r1");

      FileOutputStream fos = new FileOutputStream(dataFile);
      OutputStreamWriter ow = new OutputStreamWriter(fos, "UTF-8");
      PrintWriter pw = new PrintWriter(ow);
      CalendarGenerator.CalendarInfo info = CalendarGenerator.generateCalendarInfo();

      pw.printf(
              "0001 401 200 0 |%d| 201 0 |%s| 206 0 |%s| 208 0 |%s| 209 0 |%s| 103 0 |%s| 202 0 |%s| "
              + "203 0 |%s| 204 0 |%s| 210 0 |%s| 205 0 |%s| 207 0 |%s| 211 0 |%s| 212 0 |%s| "
              + "214 0 |%s| 215 0 |%s| 216 0 |%s| 217 0 |%s| 218 0 |%s| 219 0 |%s| 220 0 |%s| "
              + "221 0 |%s| 222 0 |%s| 223 0 |%s| 224 0 |%s| 225 0 |%s| 226 0 |%s|,",
              cd.getTemp(), ff.getNextHourSymbol(), ff.getNextHourWind(), "", "", cd.getUpdateTime(), ff.getForecastTemp(),
              ff.getForecastWind(), "", "", ff.getForecastSymbol(), ff.getForecastTimes(), ff.getNextHourPrecipitation(), "", loc,
              ff.getForecastHourlyTimes(), ff.getForecastHourlySymbol(), ff.getForecastHourlyTemp(),
              ff.getForecastHourlyPrecipitation(),
              ff.getHourlyPrecipitationChart(), ff.getHourlyTemperatureChart(),
              info.getDay(), "" + info.getDayNumber(), "" + info.getWeekNumber(), info.getMonth(), "" + info.getMonthNumber(),
              "NONEXIST_MONTH_IMAGE"
      );
      pw.println();

      pw.close();

      pw = new PrintWriter(messageFile);
      pw.printf("UPDATE,0001,,%s,%s", dataFile.getAbsolutePath(), resultFile.getAbsoluteFile());
      pw.close();
   }

   private static void addPrecipitation(Date from, Element location, Map<Date, Precipation> precepitation) {
      Element symbol = (Element) location.getElementsByTagName("symbol").item(0);
      Element precipitation = (Element) location.getElementsByTagName("precipitation").item(0);

      String symbolValue = symbol.getAttribute("id");
      String precipitationValue = precipitation.getAttribute("value");
      Precipation pre = new Precipation(symbolValue, precipitationValue);
      precepitation.put(from, pre);
   }

   private static FullForecast getPrognosis(String place) throws IOException, ParserConfigurationException, SAXException, ParseException {
      URL url = new URL("http://api.met.no/weatherapi/locationforecastlts/1.2/?" + place);

      DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

      DocumentBuilder builder = builderFactory.newDocumentBuilder();

      Document doc = builder.parse(url.openStream());

      Element weatherdata = doc.getDocumentElement();
      Element product = (Element) weatherdata.getElementsByTagName("product").item(0);
      NodeList times = product.getElementsByTagName("time");

      Map<Date, Forecast> singlePointForecast = new HashMap<>();

      Map<Date, Precipation> oneHourPrecipitation = new HashMap<>();
      Map<Date, Precipation> twoHourPrecipitation = new HashMap<>();
      Map<Date, Precipation> threeHourPrecipitation = new HashMap<>();
      Map<Date, Precipation> sixHourPrecipitation = new HashMap<>();

      for (int i = 0; i < times.getLength(); i++) {
         Element time = (Element) times.item(i);
         String fromString = time.getAttribute("from");
         String toString = time.getAttribute("to");
         Date from = DATE_FORMAT.parse(fromString);
         Date to = DATE_FORMAT.parse(toString);

         Element location = (Element) time.getElementsByTagName("location").item(0);

         if (from.equals(to)) {
            Element temperatureNode = (Element) location.getElementsByTagName("temperature").item(0);
            if (temperatureNode != null) {
               String tempString = (temperatureNode).getAttribute("value");
               String windDir = ((Element) location.getElementsByTagName("windDirection").item(0)).getAttribute("name");
               String windSpeed = ((Element) location.getElementsByTagName("windSpeed").item(0)).getAttribute("mps");
               Forecast f = new Forecast(Float.parseFloat(tempString), windDir, windSpeed, to);
               singlePointForecast.put(to, f);
            }
         } else if (to.getTime() == (from.getTime() + 1 * 3600 * 1000)) {
            addPrecipitation(from, location, oneHourPrecipitation);
         } else if (to.getTime() == (from.getTime() + 2 * 3600 * 1000)) {
            addPrecipitation(from, location, twoHourPrecipitation);
         } else if (to.getTime() == (from.getTime() + 3 * 3600 * 1000)) {
            addPrecipitation(from, location, threeHourPrecipitation);
         } else if (to.getTime() == (from.getTime() + 6 * 3600 * 1000)) {
            addPrecipitation(from, location, sixHourPrecipitation);
         }
      }

      GregorianCalendar now = new GregorianCalendar();
      GregorianCalendar nextHour = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
              now.get(Calendar.DAY_OF_MONTH), now.get(Calendar.HOUR_OF_DAY), 0);
      nextHour.add(Calendar.HOUR, 1);

      FullForecast ff = new FullForecast();
      Forecast nextHourForecast = singlePointForecast.get(nextHour.getTime());
      Precipation nextHourPrecipation = oneHourPrecipitation.get(nextHour.getTime());

      ff.setNextHourWind(getWind(nextHourForecast));
      ff.setCurrentTemp(nextHourForecast.getTemperature());
      ff.setNextHourSymbol(nextHourPrecipation.getSymbol());
      ff.setNextHourPrecipitation(nextHourPrecipation.getPrecipation());

      List<String> dayLegends = new ArrayList<>();
      List<String> symbols = new ArrayList<>();
      List<String> winds = new ArrayList<>();
      List<String> temps = new ArrayList<>();
      for (int i = 1; i < 6; ++i) {
         GregorianCalendar nextXDay = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
                 now.get(Calendar.DAY_OF_MONTH), now.get(Calendar.HOUR_OF_DAY), 0);
         nextXDay.add(Calendar.DAY_OF_MONTH, i);
         GregorianCalendar nextXDayAt12 = new GregorianCalendar(nextXDay.get(Calendar.YEAR), nextXDay.get(Calendar.MONTH),
                 nextXDay.get(Calendar.DAY_OF_MONTH), 12, 0);

         Precipation nextXDayAt12to18Precipation = sixHourPrecipitation.get(nextXDayAt12.getTime());
         Forecast nextXDayAt12Forecast = singlePointForecast.get(nextXDayAt12.getTime());

         dayLegends.add(nextXDayAt12Forecast.getTimeForDailyPres());
         symbols.add(nextXDayAt12to18Precipation.getSymbol());
         winds.add(getWind(nextXDayAt12Forecast));

         Integer max = null;
         Integer min = null;
         for (int hour = 7; hour < 20; ++hour) {
            GregorianCalendar nextXDayAtHour = new GregorianCalendar(nextXDay.get(Calendar.YEAR), nextXDay.get(Calendar.MONTH),
                    nextXDay.get(Calendar.DAY_OF_MONTH), hour, 0);
            Forecast fcHour = singlePointForecast.get(nextXDayAtHour.getTime());
            if (fcHour != null && fcHour.getTemperature() != null) {
               Integer tmp = Integer.parseInt(fcHour.getTemperature());
               if (max == null || tmp > max) {
                  max = tmp;
               }
               if (min == null || tmp < min) {
                  min = tmp;
               }
            }
         }
         if (min == null || max == null) {
            temps.add("####");
         } else {
            temps.add("" + min + "-" + max);
         }
      }

      List<String> hourLegends = new ArrayList<>();
      List<String> hourSymbols = new ArrayList<>();
      List<String> hourTemps = new ArrayList<>();
      List<String> hourPre = new ArrayList<>();
      for (int i = 1; i < 25; ++i) {
         GregorianCalendar nextXHour = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
                 now.get(Calendar.DAY_OF_MONTH), now.get(Calendar.HOUR_OF_DAY), 0);
         nextXHour.add(Calendar.HOUR, i);
         Forecast fcHour = singlePointForecast.get(nextXHour.getTime());
         Precipation preHour = oneHourPrecipitation.get(nextXHour.getTime());

         hourSymbols.add(preHour.getSymbol());
         hourTemps.add(fcHour.getTemperature());
         hourPre.add(preHour.getPrecipation());
         hourLegends.add(fcHour.getTimeForHourlyPres());
      }

      ff.setForecastTemp(StringUtils.join(temps, '#'));
      ff.setForecastSymbol(StringUtils.join(symbols, '#'));
      ff.setForecastWind(StringUtils.join(winds, '#'));
      ff.setForecastTimes(StringUtils.join(dayLegends, '#'));

      ff.setForecastHourlyPrecipitation(StringUtils.join(hourPre, '#'));
      ff.setForecastHourlySymbol(StringUtils.join(hourSymbols, '#'));
      ff.setForecastHourlyTemp(StringUtils.join(hourTemps, '#'));
      ff.setForecastHourlyTimes(StringUtils.join(hourLegends, '#'));

      ChartGenerator.BarData bd = new ChartGenerator.BarData();
      for (int i = 1; i < 49; ++i) {
         GregorianCalendar nextXHour = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
                 now.get(Calendar.DAY_OF_MONTH), now.get(Calendar.HOUR_OF_DAY), 0);
         nextXHour.add(Calendar.HOUR, i);

         Precipation preHour = oneHourPrecipitation.get(nextXHour.getTime());
         Forecast fcHour = singlePointForecast.get(nextXHour.getTime());
         if (preHour == null) {
            preHour = new Precipation("RAIN", "-1.0");
         }
         if (fcHour == null) {
            fcHour = new Forecast(0f, "N", "0", nextXHour.getTime());
         }
         bd.addPoint(preHour, nextXHour.getTime(), fcHour);
      }
      BufferedImage im = ChartGenerator.getPrecipitationChart(400, 179, bd);
      writeImageToDisk(im, "pre_chart.png");
      ff.setHourlyPrecipitationChart("pre_chart.png");
      im = ChartGenerator.getTemperatureChart(400, 179, bd);
      writeImageToDisk(im, "temp_chart.png");
      ff.setHourlyTemperatureChart("temp_chart.png");

      return ff;
   }

   private static void writeImageToDisk(BufferedImage image, String filename) {
      String pricerHome = System.getenv("PRICER_HOME");
      File fpR3Server = new File(pricerHome, "R3Server");
      File images = new File(fpR3Server, "images");
      File im = new File(images, filename);
      try {
         ImageIO.write(image, "png", im);
      } catch (NullPointerException | IOException e) {
         // thrown if the folder has DENY write access.
         throw new IllegalArgumentException("Write error, message: " + filename);
      }
   }

   private static String getWind(Forecast f1) {
      return windTranslations.get(f1.getWindDir()) + " " + Math.round(Float.parseFloat(f1.getWindSpeed())) + "m/s";
   }

}
