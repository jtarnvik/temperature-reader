/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.pricer.temperaturetopfi;

import lombok.Data;

@Data
public class Precipation {

   private final String symbol;
   private final String precipation;

   public Precipation(String symbol, String precipation) {
      this.symbol = symbol;
      this.precipation = precipation;
   }

}
