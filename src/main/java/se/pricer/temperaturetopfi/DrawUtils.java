/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.pricer.temperaturetopfi;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class DrawUtils {

   public static void writeImageToDisk(BufferedImage image, String filename) {
      try {
         ImageIO.write(image, "png", new File(filename));
      } catch (NullPointerException | IOException e) {
         // thrown if the folder has DENY write access.
         throw new IllegalArgumentException("Write error, message: " + filename);
      }
   }

   public static void fillRect(Graphics2D gr, int x, int logicalY, int width, int height, int imageHeight) {
      int y = getActualY(logicalY, imageHeight);
      gr.fillRect(x, y - height, width, height);
   }

   public static void drawLine(Graphics2D gr, int x1, int logicalY1, int x2, int logicalY2, int imageHeight) {
      int y1 = getActualY(logicalY1, imageHeight);
      int y2 = getActualY(logicalY2, imageHeight);
      gr.drawLine(x1, y1, x2, y2);
   }

   public static void drawString(Graphics2D gr, int x, int logicalY, int imageHeight, String text) {
      int y = getActualY(logicalY, imageHeight);
      gr.drawString(text, x, y);
   }

   private static int getActualY(int y, int height) {
      return height - y;
   }
}
